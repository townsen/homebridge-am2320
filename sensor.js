'use strict';

var debug = require('debug')('AM2320');
const moment = require('moment');
var os = require("os");
var hostname = os.hostname();

const delay = millis => new Promise(resolve => setTimeout(resolve, millis));
const fixed2 = number => (Math.round(number * 100) / 100).toFixed(2);
const round1 = number => Math.round(number * 10) / 10;

let Service, Characteristic;
var FakeGatoHistoryService;

module.exports = (homebridge) => {
  Service = homebridge.hap.Service;
  Characteristic = homebridge.hap.Characteristic;
  FakeGatoHistoryService = require('fakegato-history')(homebridge);

  homebridge.registerAccessory('@townsen/homebridge-am2320', 'AM2320', AM2320Plugin);
};

class AM2320Plugin {
  constructor(log, config) {
    debug('Debug enabled');
    this.log = log;
    this.name = config.name;
    this.loglevel = config.loglevel || 0;
    this.hostname = config.hostname || 'localhost'
    this.name_temperature = config.name_temperature || `${hostname}-temperature-am2320`;
    this.name_humidity    = config.name_humidity || `${hostname}-humidity-am2320`;
    this.refresh = config.refresh || 30; // Update every 30 seconds
    if (config['recordpath']) {
      this.recording = {
        size: 14400,
        storage: 'fs',
        path: config.recordpath
      }
      if (this.loglevel > 0) {
        this.log(`Recording to: ${this.recording.path}/homebridge_${this.name_temperature}_persist.json`);
      }
    }
    else {
      this.recording = {};
      if (this.loglevel > 0) {
        this.log("Not Recording");
      }
    }
    this.options = config.options || {};

    this.init = false;
    this.data = {};
    if ('i2cBusNo' in this.options) this.options.i2cBusNo     = parseInt(this.options.i2cBusNo);
    if ('i2cAddress' in this.options) this.options.i2cAddress = parseInt(this.options.i2cAddress);

    if (this.loglevel > 0) {
      this.log(`AM2320 sensor options: ${JSON.stringify(this.options)}`);
    }
    this.informationService = new Service.AccessoryInformation();

    this.informationService
      .setCharacteristic(Characteristic.Manufacturer, "Aosong")
      .setCharacteristic(Characteristic.Model, "AM2320")
      .setCharacteristic(Characteristic.SerialNumber, hostname + "-" + this.name + "-am2320")
      .setCharacteristic(Characteristic.FirmwareRevision, require('./package.json').version);

    this.temperatureService = new Service.TemperatureSensor(this.name_temperature);

    this.temperatureService
      .getCharacteristic(Characteristic.CurrentTemperature)
      .setProps({
        minValue: -100,
        maxValue: 100
      });

    this.humidityService = new Service.HumiditySensor(this.name_humidity);
    this.temperatureService.log = this.log;
    this.humidityService.log = this.log;

    this.loggingService = new FakeGatoHistoryService("weather", this.temperatureService, this.recording);
    setInterval(this.devicePolling.bind(this), this.refresh * 1000);
    this.devicePolling();
  }

  async readSensorData() {
      const sensorScriptFile = `${__dirname}/lib/read-am2320-sensor.py`;
      // eslint-disable-next-line @typescript-eslint/no-var-requires
      const spawn = require('child_process').spawn;
      const process = spawn('python3', [sensorScriptFile], { env: { LG_ADDR: this.hostname }, shell: true });
      return new Promise((resolve, reject) => {
          process.stdout.on('data', data => {
              try {
                  resolve(JSON.parse(data.toString()));
              }
              catch (err) {
                  reject(err.toString());
              }
          });
          process.stderr.on('data', err => {
              reject(err.toString());
          });
      });
  }

  devicePolling() {
    this.readSensorData()
      .then(data => {
        if (this.loglevel > 0) {
          this.log(`${fixed2(data.temperature)}°C, ` +
                   `${fixed2(data.humidity)}%`);
        }
        this.loggingService.addEntry({
          time: moment().unix(),
          temp: round1(data.temperature),
          humidity: round1(data.humidity)
        });

        this.temperatureService
          .setCharacteristic(Characteristic.CurrentTemperature, round1(data.temperature));
        this.humidityService
          .setCharacteristic(Characteristic.CurrentRelativeHumidity, round1(data.humidity));

      })
      .catch(err => {
        this.log(`AM2320 read error: ${err}`);
        if (err.stack) {
          debug(err.stack);
        }
      });
  }

  getServices() {
    return [this.informationService, this.temperatureService, this.humidityService, this.loggingService]
  }

}
