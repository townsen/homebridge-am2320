# homebridge-am2320

[AM2320](http://www.aosong.com/en/products-41.html)
temperature/humidity sensor service plugin for [Homebridge](https://github.com/nfarina/homebridge).


* Display of temperature, humidity from a AM2320 connected to a RaspberryPI.
* Support the graphing feature of the Eve app for trends

## Installation
1.	Install Homebridge using `npm install -g homebridge`
2.	Install this plugin `npm install -g @townsen/homebridge-am2320`
3.	Update your configuration file - see below for an example

## Configuration
* `accessory`: "AM2320"
* `loglevel`: 0 = none, 1 = each reading, errors are always logged
* `name`: descriptive name
* `name_temperature` (optional): descriptive name for the temperature sensor
* `name_humidity` (optional): descriptive name for the humidity sensor
* `refresh`: Optional, time interval for refreshing data in seconds, defaults to 30 seconds.
* `recordpath`: Optional, used by the fakegato history code to create an on-disk record of
  the observations. This preserves the history in case of a restart, and can be used to
  extract the observations manually.  The `name` and the `device_id` are used to name the sensor device,
  which is then used as the filename.

Note that the names given must be unique if there is more than one device of this type in
the configuration.

If you get an I/O error, make sure the I2C address is correct (usually 0x5c).

Simple Configuration

```json
{
  "bridge": {
    "name": "AM2320Example",
    "username": "CB:22:33:E2:CE:31",
    "port": 51826,
    "pin": "033-44-254"
  },
  "accessories": [
    {
      "accessory": "AM2320",
      "name": "Sensor",
      "name_temperature": "Temperature",
      "name_humidity": "Humidity",
      "options": {
        "i2cBusNo": 1,
        "i2cAddress": "0x5c"
      }
    }
  ],
  "platforms": []
}
```

This plugin creates two services: TemperatureSensor and HumiditySensor.

## Credits

This was adapted from the two projects below:
* homebridge-bme280
* homebridge-am2320

## License

MIT
